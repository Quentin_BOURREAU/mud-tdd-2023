public class Executable{

    public static void main(String [] args){
        Box b = new Box();
        Thing truc = new Thing("truc");
        b.setCapacity(10);
        truc.setVolume(4);
        b.actionAdd(truc);
        System.out.println(b.actionLook());
        b.close();
        System.out.println("fermeture de la boite");
        System.out.println(b.actionLook());
    }
}