# MUD TDD 2023

## Présentation du projet et de ses fonctionnalités
#

Dans notre projet, on y retrouve une classe Thing qui désigne une chose quelconque qui a un nom et un volume. À partir de cette Thing, on peut regarder ses attributs par exemple savoir si elle se nomme d'une certaine façon ou encore savoir son volume. Par la suite, il y a une classe Box qui contient donc une liste de Thing (choses). Cette boite a une capacité, c'est-à-dire qu'elle ne peut pas contenir plus de choses que sa capacité, mais aussi un booléen pour savoir si elle est ouverte ou fermée. Ce dernier attribut permet de savoir si on peut regarder ce qu'il y a dedans. De plus, on peut même regarder si cette boite (sachant qu'elle est ouverte au préalable) contient une Thing particulière ou non. Nous avons aussi rajouté une méthode pour permettre de lire les fichiers json et de récupérer les attributs d'une boite définie en json et pouvoir ensuite réaliser les mêmes tests qu'avec un exécutable normal. De plus, nous avons par la suite, et ce fut une grande partie du travail, créé une classe TestsBoxes permettant de tester toutes ces fonctionnalités en utilisant le principe des tests (@Test) en junit. Dedans, nous y retrouvons les tests classiques en junit, les tests de lecture d'un fichier json, mais aussi les tests d'exception. Pour finir, nous avons aussi rajouté un court exécutable pour notre projet, même s'il y en a déjà un dans le projet fusionné avec maven car nous l'avions ajouté avant cela.

## Fusionnement du projet avec maven et traitement des fichiers en Java
#

Une fois le projet fini, nous avons suivi notre enseignant pour fusionner le projet avec maven et traiter par la suite les fichiers en Java. Ce fusionnement avait pour objectif de mieux ranger nos fichiers Java et de comprendre le fonctionnement de maven. Par conséquent, nous avons donc créé un projet maven de base. Ensuite, nous avons mis notre projet MUD sous la forme de maven souhaité. Après cela, nous avons initialisé maven avec un package précis et mis en place le projet dans l'arborescence maven. Ensuite, nous avons ajouté des règles à maven pour la librairie de gson. Enfin, nous avons fini par ajouter le plugin  maven-exec-plugin pour pouvoir exécuter directement l'App au lieu de l'exécutable d'avant.


## Ce que nous avons appris pendant ce projet
#

L'objectif principal de ce projet a été d'utiliser et de maîtriser les branches en git pour savoir travailler dans une équipe sans qu'il n'y est trop de problèmes de collision entre les fichiers. Ensuite, ce projet a été l'occasion de nous entraîner au développement dirigé par les tests avec junit (@Test). Nous avons utilisé également les exceptions pour des tests, ce que nous n'avions pas encore appris à ce moment donné. Puis, ce projet a permis également d'apprendre à utiliser des packages,mais également les bases de maven.


#
BOURREAU Quentin, JACQUET Noa, BALLUS Hugo