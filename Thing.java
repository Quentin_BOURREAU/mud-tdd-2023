public class Thing{

    private String name;
    private int volume;


    /**
     * Instancie une chose avec un nom et un volume
     * 
     * @param name (String) : nom 
     */
    public Thing(String name){
        this.name = name;
        this.volume = 0;
    }
    /**
     * Instancie une chose avec un nom et un volume
     * 
     * @param name (String) : nom 
     * @param volume (int) : volume
     */
    public Thing(String name, int volume){
        this.name = name;
        this.volume = volume;
    }

    /**
     * Renvoie le nom de la chose
     * 
     * @return String: nom de la chose
     */
    public String getName(){
        return this.name;
    }

    public int getVolume(){
        return this.volume;
    }

    /**
     * Renvoie le volume de la chose
     * 
     * @return int: volume de la chose
     */
    public void setVolume(int volume){
        this.volume = volume;
    }

    /**
     * Renvoie un affichage de la chose
     * 
     * @return String 
     */
    @Override
    public String toString(){
        return this.name;
    }
    
    /**
     * Vérifie si la chose est égale à l'objet en paramètre
     * 
     * @param obj (Object) : un objet
     */
    @Override
    public boolean equals(Object obj){
        if (obj instanceof Thing){
            return ((Thing)obj).getName().equals(this.name);
        }
        else{
            return false;
        }
    }

    /**
     * Change le nom de la chose
     * 
     * @param nouveauNom (String) : nouveau nom de la chose
    */
    public void setName(String nouveauNom){
        this.name = nouveauNom;
    }

    /**
     * Permet de savoir si le  nom en paramètres est bien celui de la chose 
     * @param nomThing (String) : nom à vérifier
     * @return boolean : true si le nom est bien celui de la chose false sinon
    */
    public boolean hasName(String nomThing){
        return this.name.equals(nomThing);
    }
}