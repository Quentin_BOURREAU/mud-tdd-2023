import java.io.FileReader;
import java.io.FileNotFoundException;
import com.google.gson.Gson;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/** Classe Box avec création et add */
public class Box{
    /** Contenu de la Box */
    private List<Thing> contents = new ArrayList<>();

    private boolean ouvert;

    private int capacity;

    /**
     * Instancie une boite à ouvert et de capacité -11
     */
    public Box(){
        System.out.println("Box cree");
        this.ouvert = true;
        this.capacity = -1;
    }

    /**
     * Instancie une boite à partir d'un booléan qui désigne si elle est ouverte ou fermée
     * 
     * @param ouvert (boolean) : true si la boite est ouverte, sinon false
     */
    public Box(boolean ouvert){
        this.ouvert = ouvert;
        this.capacity = -1;
        System.out.println("Box cree");
    }

    /**
     * Instancie une boite à partir d'un booléan qui désigne si elle est ouverte ou fermée et de sa capacité
     * 
     * @param ouvert (boolean) : true si la boite est ouverte, sinon false
     * @param capacity (int) : la capacité de la boite
     */
    public Box(boolean ouvert, int capacity){
        this.ouvert = ouvert;
        this.capacity = capacity;
        System.out.println("Box cree");
    }

    /**
     * Retourne la capacité de la boite instanciée
     * 
     * @return int : la capacité de la boite
     */
    public int capacity(){
        return this.capacity;
    }

    /**
     * Modifie la capacité de la boite instanciée
     * 
     * @param capacity (int) : la nouvelle capacité de la boite
     */
    public void setCapacity(int capacity){
        this.capacity = capacity;
    }

    /**
     * Ajoute une chose à la liste de choses (contents) de la boite instanciée à partir d'un String
     * 
     * @param truc (String) : la chose à ajouter sous forme de String
     */
    public void add(String truc){
        this.contents.add(new Thing(truc));
    }

    /**
     * Ajoute une chose à la liste de choses (contents) de la boite instanciée à partir d'un Thing
     * 
     * @param truc (Thing) : la chose à ajouter sous forme de Thing
     */
    public void add(Thing truc){
        this.contents.add(truc);
    }

    /**
     * Savoir si la boite instanciée contient la chose à partir d'un String
     * 
     * @param truc (String) : la chose à chercher sous forme de String
     * @return boolean : true si la chose est dans la boite, sinon false
     */
    public boolean contientTruc(String truc){
        return this.contents.contains(new Thing(truc));
    }

    /**
     * Savoir si la boite instanciée contient la chose à partir d'un Thing
     * 
     * @param truc (Thing) : la chose à chercher sous forme de Thing
     * @return boolean : true si la chose est dans la boite, sinon false
     */
    public boolean contientTruc(Thing truc){
        return this.contents.contains(truc);
    }

    /**
     * Retourne le contenu de la boite instanciée
     * 
     * @return List : la liste de choses contenues dans la boite
     */
    public List<Thing> getContents(){
        return this.contents;
    }

    /**
     * Savoir si la boite est ouverte ou non
     * 
     * @return boolean : true si la boite est ouverte, sinon false
     */
    public boolean isOpen(){
        return this.ouvert;
    }

    /**
     * Ouvre la boite
     */
    public void open(){
        this.ouvert = true;
    }

    /**
     * Ferme la boite
     */
    public void close(){
        this.ouvert = false;
    }

    /**
     * Regarde ce qu'il y a dans la boite instanciée
     * 
     * @return String : le contenu de la boite si elle est ouverte sinon un message pour dire qu'elle est fermée
     */
    public String actionLook(){
        if (this.ouvert){
            String contenuBoite = "";
            for (Thing truc : this.contents){
                contenuBoite = contenuBoite + truc.toString() + ", ";
            }
            return "La boite contient : " + contenuBoite;
        }
        return "La boite est fermée";
    }

    /**
     * Ajoute une chose à la boite instanciée s'il y a de la place (capacité pas atteinte) et si elle est ouverte
     * 
     * @param chose (Thing) : la chose à ajouter
     * @return boolean : true si la chose a été ajoutée, sinon false
     */
    public boolean actionAdd(Thing chose){
        if (this.hasRoomFor(chose) && this.isOpen()){
            this.contents.add(chose);
            this.capacity -= chose.getVolume();
            return true;
        }
        return false;
    }

    /**
     * Retire une chose de la boite instanciée
     * 
     * @param truc (String) : la chose à retirer
     */
    public void retirerTruc(String truc){
        try{
            Thing chose = new Thing(truc);
            this.contents.remove(chose);
            this.capacity += chose.getVolume();
        }
        catch (Exception e){
            System.out.println(truc + " n'existe pas dans la box !");
        }
    }

    /**
     * Retourne un affichage du contenu de la boite
     * 
     * @return String : le contenu de la boite
     */
    @Override
    public String toString(){
        String boite = "";
        for (Thing truc : this.contents){
            boite = boite + " " + truc.getName();
        }
        return boite;
    }    
    
    /**
     * Enleve une chose de la boite instanciée avec un processus d'exception
     * 
     * @param truc (Thing) : la chose à retirer
     * @throws RuntimeException
     */
    public void remove(Thing truc) throws RuntimeException{
        boolean ok = this.contents.remove(truc);
        if (!ok) throw new RuntimeException("Remove impossible!");
    }

    /**
     * Savoir si la boite aurait assez de place pour mettre la chose
     * 
     * @param chose (Thing) : la chose potentielle à ajouter
     * @return boolean : true si on pourrait ajouter la chose, sinon false
     */
    public boolean hasRoomFor(Thing chose){
        if (this.capacity >= chose.getVolume() || this.capacity == -1){
            return true;
        }
        return false;
    }

    /**
     * Lit un fichier json et affiche le contenu de la boite du fichier
     * 
     * @param nomFic (String) : le nom du fichier json
     * @return Box : la boite lue dans le fichier
     */
    public static Box fromJSON(String nomFic){
        try{
            FileReader fr = new FileReader(nomFic);
            Gson json = new Gson();
            Box maBoite = json.fromJson(fr, Box.class);
            return maBoite;
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Cherche si la chose est dans la boite instanciée
     * 
     * @param nom (String) : le nom de la chose à chercher
     * @return Thing : la chose de type Thing
     * @throws RuntimeException
     */
    public Thing find(String nom) throws RuntimeException{
        if (!this.isOpen()) throw new RuntimeException("La boite est fermée");
        for (Thing truc : this.contents){
            if (nom == truc.getName()){
                return truc;
            }
        }
        throw new RuntimeException("Chose introuvable");
    }
}