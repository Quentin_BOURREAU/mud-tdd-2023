import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.util.List;

public class TestsBoxes{
    @Test
    public void testBoxCreate() {
    Box b = new Box();
    }

    @Test
    public void testBoxAdd(){
        Box b = new Box();
        b.add("item1");
        b.add("item2");
    }

    @Test
    public void testBoxRetirerTruc(){
        Box b = new Box();
        b.add("item1");
        b.add("item2");
        System.out.println(b); // retourne item1 item2
        b.retirerTruc("item2");
        System.out.println(b); // retourne item1
        b.retirerTruc("item5"); // affiche l'exception de la méthode retirerTruc
    }

    @Test
    public void testContient(){
        Box b = new Box();
        b.add("item");
        b.contientTruc("item");
    }

    @Test
    public void testBoxContains(){
        Box b = new Box();
        Thing t = new Thing("chose");
        b.add(t);
        assertTrue(b.getContents().contains(t));
        assertTrue(b.contientTruc(t));
        Thing truc = new Thing("truc");
        assertFalse(b.contientTruc(truc));
        Box b2 =new Box();
        assertFalse(b2.contientTruc(t));
        assertFalse(b2.contientTruc(truc));
    }

    @Test(expected = RuntimeException.class)
    public void testBoxRemoveFail(){
        Thing truc = new Thing("truc");
        Box b = new Box();
        b.remove(truc);
    }

    @Test
    public void testBoxRemoveOk(){
        Thing truc = new Thing("truc");
        Box b = new Box();
        b.add("truc");
        b.remove(truc);
    }

    @Test
    public void testOuvertureBox(){
        Thing chose = new Thing("truc");
        Box b = new Box(false);
        assertFalse(b.isOpen());
        b.add(chose);
        b.add("MUD");
        assertFalse(b.actionLook().equals("La boite est ouverte"));
        b.open();
        assertTrue(b.isOpen());
        assert b.actionLook().equals("La boite contient : truc, MUD, ");
        b.close();
        assertFalse(b.isOpen());
    }

    @Test
    public void testNameThing(){
        Thing truc = new Thing("truc");
        assertTrue(truc.getName().equals("truc"));
        truc.setName("chose");
        assertFalse(truc.getName().equals("truc"));
        assertTrue(truc.hasName("chose"));
    }

    @Test
    public void testBoxFromJSONSimple(){
        Box b = Box.fromJSON("test1.json");
        System.out.println("Boite chargée de capacité :" + b.capacity());
    }

    @Test
    public void testBoxFromJSONComplet(){
        Box b = Box.fromJSON("test2.json");
        System.out.println("Boite chargée de capacité :" + b.capacity());
        assertEquals(b.actionLook(), "La boite contient : truc, machin, bidule, ");
        assertEquals(b.capacity(), -1);
    }

    @Test
    public void testBoxFromJSONComplet2(){
        Box b = Box.fromJSON("test3.json");
        System.out.println("Boite chargée de capacité :" + b.capacity());
        assertEquals(b.actionLook(), "La boite contient : truc, machin, bidule, ");
        assertEquals(b.capacity(), -1);
        List<Thing> contents = b.getContents();
        for (Thing thingActuelle : contents){
            assertEquals(thingActuelle.getVolume(), 10);
        }
    }

    @Test
    public void testVolumeThing(){
        Thing truc = new Thing("truc");
        assertEquals(truc.getVolume(), 0);
        truc.setVolume(5);
        assertEquals(truc.getVolume(), 5);
    }

    @Test
    public void testCapaciteBoite(){
        Box b = new Box();
        assertEquals(b.capacity(), -1);
        b.setCapacity(20);
        assertEquals(b.capacity(), 20);
    }

    @Test 
    public void testHasRoomFor(){
        Box b = new Box();
        Thing truc = new Thing("truc");
        truc.setVolume(5);
        b.setCapacity(10);
        assertTrue(b.hasRoomFor(truc));
    }

    @Test 
    public void testActionAdd(){
        Box b = new Box();
        Thing truc = new Thing("truc");
        Thing truc2 = new Thing("truc2");
        truc.setVolume(5);
        truc2.setVolume(6);
        b.setCapacity(10);
        assertTrue(b.actionAdd(truc));
        assertFalse(b.actionAdd(truc2));
    }

    @Test
    public void testFind1(){
        Box b = new Box();
        Thing truc = new Thing("truc");
        b.actionAdd(truc);
        assertEquals(b.find("truc"), truc);
    }

    @Test(expected = RuntimeException.class)
    public void testFind2(){
        Box b = new Box();
        Thing truc = new Thing("truc");
        assertEquals(b.find("truc"), truc);
    }
}